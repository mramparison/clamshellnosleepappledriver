#include <IOKit/IOService.h>
#include <IOKit/IOLib.h>
#include <IOKit/pwr_mgt/RootDomain.h>

class com_mydriver : public IOService

{

    OSDeclareDefaultStructors(com_mydriver)

public:

    virtual bool init(OSDictionary *dictionary = 0);

    virtual void free(void);

    virtual IOService *probe(IOService *provider, SInt32 *score);

    virtual bool start(IOService *provider);

    virtual void stop(IOService *provider);

};
